import time
from OpenGL.GL import *
import math
from cm import *
from autograff.math_utils import *
from autograff.geom_base import *
from autograff.gfx import *
from autograff.beta_lognormal import *

from ann import *
from render_utils import *

import app

# converts a sequence of points to a turning-angle distance pair
# the first pair will have turning angle 0
# and all distances will be normalized by dividing them by the total distance
# maybe should use mean dist?


# lets test it
#net1.fit(X,Y)

#print X
#print Y
    #plotit()
    #app.deferred_call(plotit)

def load_shapes(path):

    shapes = shapes2dFromXml(path)
    shapes = [s for s in shapes]
    r = Rect(0,0,500,500)
    for s in shapes:
        box = s.calcBoundingBox()
        m = getFittingMatrix(box,r,True)
        #m.identity()
        #m.scale(700.0/64)
        s.transform(m)

    return shapes
    


pmaker = PathMaker()
pmaker.unpickle('path.pth')

ann = Ann()
ann.unpickle()

brush_img = Image('brush.png')

rt = RenderTarget(app.width(),app.height())

class App:
    def __init__(self):
        app.setParamPath("Params")
        app.addBool('render',False)
        app.addFloat('render w',100,20,300)
        app.addEvent('clear render',self.clear_render)

        app.addFloat('seed',0.1,0,2)
        app.addFloat('curshape',0,0,1)
        app.addFloat('simplify',1,0,30)
        app.addEvent('clear',self.clear)
        app.addColor("bg",Color(1,0,1,1))
        app.addAsyncEvent('open...',self.open)
        app.addString('glyph file','glyphs.xml')
        app.setParamPath('Brush')
        app.addBool('Use Brush',True)
        app.addFloat('Brush Width',3.0,0.0,100.0)
        app.addFloat("Min Velocity",20.0,0.1,100.0)
        app.addFloat("Max Velocity",3500,100.0,5000.0)
        app.addFloat("Brush Spread",3.0,0.01,5.0)
        
        app.addFloat("Drip Thresh",0.2, 0.0, 2.0)
        app.addFloat("Drip Length",100,0,300)

        add_beta_lognormal_params()

        self.Dt = None
        self.Beta = None

        self.shapes = load_shapes(app.getString('glyph file'))

        self.render_start = Vec2(20,20)
        self.render_pos = Vec2(self.render_start)
        self.render_index = 0

    def clear(self):
        pmaker.clear()
        self.Dt = None
        self.Beta = None

    def open(self):
        p = app.openFileDialog('xml')
        if p:
            self.shapes = load_shapes(p)
            app.setString('glyph file', p)

    def init(self):
        self.clear_render()
        print "File: " + app.getString('glyph file')
        print "Innit"

    def exit(self):
        pmaker.pickle('path.pth')

    def draw_fancy( self, P, V, CI ):
        wmul = 1.0
        if getb('render'):
            wmul = 1.0

        color(0.0)
        draw_with_brush( P, V, CI, 
            brush_img, 
            brush_width = getf('Brush Width')*wmul,
              brush_min_velocity = getf('Min Velocity'),
              brush_max_velocity = getf('Max Velocity'),
              brush_spread = getf('Brush Spread'),
              drip_thesh = getf('Drip Thresh'),
              drip_length = getf('Drip Length'),
              clr = Color(0.0) )

    def compute_shape( self, shape ):
        for P in shape:
            self.compute_trajectory(P)

    def nudge(self, start_pos, S, target, dt = 0.001, eps=1.0, debug=False):
        P, V, VS, CI, endt = beta_lognormal( start_pos, S, dt=dt )
        if debug:
            draw_np(P)
        CP = get_critical_points(CI, P)
        S = nudge_waypoints(S, CP, target, eps=eps, debug=debug )   

        return S

    def compute_trajectory(self, P):
        Dt, Beta = ann.predict(P)
        # if app.mouseClicked(0):
        #     self.clear()
        #     return

        WP = path2d_to_np( P )
        start_pos, S =  stroke_vector_from_way_points(WP, params=get_beta_lognormal_params() )
        set_param_in_stroke_vector(BL_deltaT, S, Dt )
        set_param_in_stroke_vector(BL_beta, S, Beta )
        P, V, VS, CI, endt = beta_lognormal(start_pos, S)

        for i in range(3):
            S = self.nudge( start_pos, S, WP )
        
        P, V, VS, CI, endt = beta_lognormal(start_pos, S)

        if getb('Use Brush'):
            self.draw_fancy(P, V, CI)
        else:
            color(1,0,0)
            draw_np(P)
        
    def clear_render(self):
        rt.bind()
        clear(1,1,1,1)
        self.render_index = 0
        self.render_pos = Vec2(self.render_start)
        color(1)
        fillRect(0,0,rt.getWidth(),rt.getHeight())
        rt.unbind()

    def render(self):
        

        clear(0,0,0,1)
        setBlendMode(BLENDMODE_ALPHA)
        color(1.0)
        rt.getTexture().draw(0,0,app.width(),app.height(),True)
        
        if not len(self.shapes):
            return

        if self.render_index >= len(self.shapes):
            return

        s = Shape2d(self.shapes[self.render_index])
        s.simplify(getf('simplify'))

        rw = getf('render w')
        w = 1024 #app.width()

        rt.bind()
        pushMatrix()

        s.simplify(getf('simplify'))
        
        translate(self.render_pos.x, self.render_pos.y)
        scale(rw/500)
        if getb('Use Brush'):
            self.compute_shape(s)
        else:
            color(0,0,0)
            s.draw()

        popMatrix()

        self.render_pos.x += rw

        if self.render_pos.x > (w-rw):
            #print "Restarting at " + str(self.render_start.x)
            self.render_pos.x = self.render_start.x
            self.render_pos.y += rw
        # assume 64

        rt.unbind()

        

        self.render_index = self.render_index + 1

    def frame(self):
        ishape = int(getf('curshape') * (len(self.shapes)-1))
        #print len(shapes)

        r = Rect(0,0,500,500)

        setBlendMode(BLENDMODE_ALPHA)
        random_seed(getf('seed'))
        clear(getc('bg'))
        
        if getb('render'):
            self.render()
            return

        if len(self.shapes): #pmaker.is_valid():
            s = Shape2d(self.shapes[ishape])
            s.simplify(getf('simplify'))
            
            pushMatrix()
            translate(100,100)
            color(0.0,0.4)
            s.draw()
            
            if ann.ready:
                self.compute_shape( s )

            popMatrix()

            #self.predict()
            #self.compute_trajectory()
        #if self.Dt and pmaker.is_valid():
            
