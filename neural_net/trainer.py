import time
from OpenGL.GL import *
import math
from cm import *
from autograff.math_utils import *
from autograff.geom_base import *
from autograff.gfx import *
from autograff.beta_lognormal import *

from ann import *
from render_utils import *

import app

# converts a sequence of points to a turning-angle distance pair
# the first pair will have turning angle 0
# and all distances will be normalized by dividing them by the total distance
# maybe should use mean dist?


# lets test it
#net1.fit(X,Y)

#print X
#print Y
    #plotit()
    #app.deferred_call(plotit)

pmaker = PathMaker()
pmaker.unpickle('path.pth')

ann = Ann()
ann.unpickle()

brush_img = Image('brush.png')

class App:
    def __init__(self):
        app.setParamPath("Params")
        app.addFloat('seed',0.1,0,2)
        app.addEvent('clear',self.clear)
        app.addColor("bg",Color(1,0,1,1))
        app.addAsyncEvent('open...',self.open)
        app.addAsyncEvent("train",ann.train)
        app.addEvent('compute',self.predict)
        app.setParamPath('Brush')
        app.addBool('Use Brush',True)
        app.addFloat('Brush Width',3.0,0.0,100.0)
        app.addFloat("Min Velocity",20.0,0.1,100.0)
        app.addFloat("Max Velocity",3500,100.0,5000.0)
        app.addFloat("Brush Spread",3.0,0.01,5.0)
        
        app.addFloat("Drip Thresh",0.2, 0.0, 2.0)
        app.addFloat("Drip Length",100,0,300)

        add_beta_lognormal_params()

        self.Dt = None
        self.Beta = None

    def clear(self):
        pmaker.clear()
        self.Dt = None
        self.Beta = None

    def open(self):
        p = app.openFileDialog('txt')
        if p:
            print p
            
    def predict(self):
        self.Dt, self.Beta = ann.predict( pmaker.get_path() )

    def init(self):
        print "Innit"

    def exit(self):
        pmaker.pickle('path.pth')

    def draw_fancy( self, P, V, CI ):
        color(0.0)
        draw_with_brush( P, V, CI, 
            brush_img, 
            brush_width = getf('Brush Width'),
              brush_min_velocity = getf('Min Velocity'),
              brush_max_velocity = getf('Max Velocity'),
              brush_spread = getf('Brush Spread'),
              drip_thesh = getf('Drip Thresh'),
              drip_length = getf('Drip Length'),
              clr = Color(0.0) )

    def compute_trajectory(self):

        # if app.mouseClicked(0):
        #     self.clear()
        #     return

        WP = path2d_to_np( pmaker.get_path() )
        start_pos, S =  stroke_vector_from_way_points(WP, params=get_beta_lognormal_params() )
        set_param_in_stroke_vector(BL_deltaT, S, self.Dt )
        set_param_in_stroke_vector(BL_beta, S, self.Beta )
        P, V, VS, CI, endt = beta_lognormal(start_pos, S)

        if getb('Use Brush'):
            self.draw_fancy(P, V, CI)
        else:
            color(1,0,0)
            draw_np(P)
        


    def frame(self):
        setBlendMode(BLENDMODE_ALPHA)
        random_seed(getf('seed'))
        clear(getc('bg'))
        color(0.0,0.4)
        pmaker.update()
        pmaker.shape.draw()
        
        if ann.ready and pmaker.is_valid():
            self.predict()
            self.compute_trajectory()
        #if self.Dt and pmaker.is_valid():
            
