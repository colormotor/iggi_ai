
import time
from OpenGL.GL import *
import math
from cm import *
from autograff.math_utils import *
from autograff.geom_base import *
from autograff.gfx import *
from autograff.beta_lognormal import *

import xml.etree.ElementTree as ET
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
import random
from lasagne import layers
from lasagne.updates import nesterov_momentum
from nolearn.lasagne import NeuralNet



def feature_vector_size( n_history, n_future ):
    return n_history*4 + 2 + n_future*2


def feature_vector_for( A, L, Dt, Beta, i, n_history, n_future ):
    X = []
    # loop back 
    for j in range( 1, n_history+1 ):
        il = i-j
        if il >= 0:
            # add angle and length
            X.append(A[il])
            X.append(L[il])
            X.append(Dt[il])
            X.append(Beta[il])
        else:
            X.append(0.0) # set to zero for boundary points
            X.append(0.0)
            X.append(0.0)
            X.append(0.0)

    # now current sample, only A,L
    X.append(A[i])
    X.append(L[i])

    n = len(A)

    # future samples but without SLM params
    for j in range( 1, n_future+1 ):
        ir = i+j
        if ir < n:
            X.append(A[ir])
            X.append(L[ir])
        else:
            X.append(0.0)
            X.append(0.0)

    return X


def points_to_turn_dist_pairs( WP ):
    D = [b-a for a,b in zip(WP, WP[1:])]
    A = np.array([0.0] + [angleBetween(a,b) for a,b in zip(D, D[1:])])
    L = np.array([d.length() for d in D ])
    L = L / np.sum(L)
    return A, L

def training_data_from_trajectory( traj, n_history, n_future ):
    A, L = points_to_turn_dist_pairs( traj['WP'] )
    Dt = [ s['dt'] for s in traj['S'] ]
    Beta = [ s['beta'] for s in traj['S'] ]

    n1 = len(Dt)
    n = len(A)

    X = []
    Y = []

    for i in range(n):
        X.append( 
            feature_vector_for( A, L, Dt, Beta, i, n_history, n_future ) 
        )
        
        # Y alues
        Y.append([Dt[i], Beta[i]])

    print "Y Vector"
    print Y

    return X, Y

def build_training_data( buf, n_history, n_future ):
    X = []
    Y = []
    for traj in buf:
        x,y = training_data_from_trajectory(traj, n_history, n_future)
        X += x
        Y += y
    
    return np.array(X, dtype=np.float32), np.array(Y, dtype=np.float32)

def replicate_with_noise( data, n, pos_offset=5.0, dt_offset=0.05, beta_offset=0.1 ):
    res = []

    def randv(v):
        return Vec2(frand(-v,v), frand(-v,v))

    for traj in data:
        res.append(traj)

        # create n copies with offseted parameters
        for i in range(n):
            t2 = {}
            WP = traj['WP']
            WP2 = [ p+randv(pos_offset) for p in WP ]
            S = traj['S']
            S2 = []
            for s in S:
                dt2 = s['dt'] + frand(-dt_offset, dt_offset)
                beta2 = s['beta'] * frand(1.0-beta_offset,1.0+beta_offset)
                S2.append( {'dt':dt2, 'beta':beta2} )
            
            t2['WP'] = WP2
            t2['S'] = S2

            res.append(t2)

    return res

def load_training_data( path, n_history, n_future ):
    try:
        tree = ET.parse(path)
    except:
        print "Could not parse xml file " + path
        return

    root = tree.getroot()

    res = []

    # convert Xml to something more sensible
    for child in root:
        traj = {}
        node = child.find('WayPoints')
        WP = []
        for c in node:
            WP.append(Vec2(float(c.attrib['x']),
                           float(c.attrib['y'])))
        traj['WP'] = WP

        node = child.find('Strokes')
        S = []
        for c in node:
            S.append( {'dt': float(c.attrib['dt']),
                        'beta': float(c.attrib['beta']) } )
            traj['S'] = S
        res.append(traj)

    # create noisy version for generalization
    print len(res)
    res = replicate_with_noise(res, 5)
    print len(res)
    # shuffle
    random.shuffle(res)

    return build_training_data( res, n_history, n_future )



n_history = 2
n_future = 1

class Ann:
    def __init__(self):
        self.net1 = NeuralNet(
            layers=[ # three layers one hidden
                ('input', layers.InputLayer ),
                ('hidden', layers.DenseLayer),
                ('output', layers.DenseLayer)
            ],

            input_shape=(None, feature_vector_size( n_history, n_future ) ),
            hidden_num_units=15,
            output_nonlinearity=None,
            output_num_units=2,

            #optimization
            update=nesterov_momentum,
            update_learning_rate=0.01,
            update_momentum=0.9,

            regression=True,

            max_epochs=700,
            verbose=1
            )

        self.ready = False

    def train(self):
        self.X, self.Y = load_training_data('training.xml', n_history, n_future)

        self.net1.fit(self.X,self.Y)
        
        self.pickle()

        self.ready = True

    def pickle(self):
        app.pickle_to_file('net1.pickle',self.net1)

    def unpickle(self):
        try:
            if os.path.isfile('net1.pickle'):
                self.net1 = app.pickle_from_file('net1.pickle')
                self.ready = True
        except:
            print "Could not load nn"


    def plot():
        fig = Figure()
        ax = fig.add_subplot(111)

        train_loss = np.array([i["train_loss"] for i in self.net1.train_history_])
        valid_loss = np.array([i["valid_loss"] for i in self.net1.train_history_])
        ax.plot(train_loss, linewidth=3, label="train")
        ax.plot(valid_loss, linewidth=3, label="valid")
        ax.grid()
        ax.legend()
        ax.set_xlabel("epoch")
        ax.set_ylabel("loss")
        ax.set_ylim(-3.0, 3.0)
        ax.set_yscale("log")
        app.plot_figure(fig)

    def predict(self, path, angle_thresh = 50.0, verbose=False):
        angle_thresh = radians(angle_thresh)

        WP = [p for p in path]
        A, L = points_to_turn_dist_pairs(WP)
        
        Beta = []
        Dt = []

        for i in range(len(A)):
            x = feature_vector_for( A, L, Dt, Beta, i, n_history, n_future )
            x = np.array([x], dtype=np.float32 )

            y = self.net1.predict( np.array(x, dtype=np.float32) )

            if verbose:
                print "Point %d predictions"%i
                print y

            Dt.append(y[0][0])
            Beta.append(y[0][1])

        for i in range(len(A)):
            a = A[i]
            dt = Dt[i]
            
            if abs(a) < angle_thresh:
                Dt[i] = 0.07
                p = WP[i]
                color(1,0,0)
                drawText(p.x,p.y,"%g"%degrees(a))

        return Dt, Beta
        

