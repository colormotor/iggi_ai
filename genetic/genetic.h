#pragma once
#include "colormotor.h"
#include "numeric.h"
#include "Shape.h"
#include "ocr.h"

class Env;

///////////////////////////////////////////////////
///////////////////////////////////////////////////

class Genotype
{
public:
    Genotype( Env * env );
    virtual ~Genotype() {}
    
    void randomize();
    
    Vec2 mutatePos( const Vec2 & p, float amt  );
    void mutate( float amt );
    Genotype crossoverSingle( const Genotype & mate ) const;
    Genotype crossoverMulti( const Genotype & mate ) const;
    vector2f randomRow();
    
    Env * env;
    matrix2f genes;
};

///////////////////////////////////////////////////
///////////////////////////////////////////////////

class Glyph
{
public:
    Glyph( Env * env );
    ~Glyph();
    
    float fittness();
    void update();
    
    void draw();
    
    Env * env;
    
    Image img;
    
    float confidence;
    
    Shape strokes;
    Genotype gen;
};

///////////////////////////////////////////////////
///////////////////////////////////////////////////

class Population
{
public:
    Population( Env * env );
    ~Population();
    
    void fitness();
    void selection();
    void reproduction();
    
    void update();
    
    void reset();
    
    void draw();
    
    Env *env;
    std::vector<Glyph> elements;
    std::vector<int> matingPool;
    
    float bestFitness;
    Genotype bestGen;
    
    int generations = 0;
    
    vectorf F;
    vectorf fmeanHistory;
    vectorf fmaxHistory;
    
    static const int numElements = 50;
};

///////////////////////////////////////////////////
///////////////////////////////////////////////////

/***********
Trix:
 Elitims-Truncation:
    N fittest ind. are selected
    Elitism -> exact copy goes into new gen
        age constraint -> remove after M generations
    Truncation -> top individuals have children
    (May coverge early due to limited gene pool)
 
 Fittness Propportionate selection
    Select n with a prob p(i) proportionate to fittness
        p(i) = f(i) / sum(f(j))
    gives unfit ind. chance of selection
 
 
*///////////
class Env
{
public:
    Env();
    
    bool init();
    void update();
    void reset();
    void drawChoices( float x, float y, float canvasw );
    void drawChoicesToEps( const std::string & path, float x, float y, float canvasw );
    
    ParamList params;
    
    int minStrokes = 2;
    int maxStrokes = 10;
    int minStrokePts = 2;
    int maxStrokePts = 10;
    float mutateAmt = 0.1;
    float mutateProb = 0.5;
    float lineWidth = 7.0;
    char characters[50];
    float confPower = 9.0;
    float minAreaScale = 0.5;
    float blurAmt=1.0;
    int subdivisionX=10;
    int subdivisionY=10;
    float choiceThresh=0.5;
    Trigger<bool> reset_;
    float plotScale=2.0;
    
    std::vector<Shape> choosen;
    
    Rectf rect;
    Population population;
    OCR ocr;
};

