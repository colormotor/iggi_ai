//
//  ocr.cpp
//  iggi_ai_gen
//
//  Created by Daniel Berio on 10/03/15.
//  Copyright (c) 2015 colormotor. All rights reserved.
//

#include "ocr.h"

using namespace std;

OCR::OCR()
{
    
}
void OCR::setup(string dataPath, bool absolute, string language) {
    // there is some kind of bug in Init that ignores the datapath argument
    // so we override it by setting an environment variable
//    setenv("TESSDATA_PREFIX", dataPath.c_str(), 1);
    tess.Init(dataPath.c_str(), language.c_str());
    
    // fixes issues with hocr - see http://code.google.com/p/tesseract-ocr/issues/detail?id=463
    tess.SetInputName("");
    setMode(AUTO);
}


void OCR::setWhitelist(string whitelistCharacters) {
    tess.SetVariable("tessedit_char_whitelist", whitelistCharacters.c_str());
}

void OCR::setBlacklist(string blacklistCharacters) {
    tess.SetVariable("tessedit_char_blacklist", blacklistCharacters.c_str());
}

void OCR::setMode(Mode mode) {
    tess.SetPageSegMode((tesseract::PageSegMode) mode);
}

OCR::Mode OCR::getMode(string modeName) {
    if(modeName == "AUTO") {
        return AUTO;
    } else if(modeName == "COLUMN") {
        return COLUMN;
    } else if(modeName == "BLOCK") {
        return BLOCK;
    } else if(modeName == "LINE") {
        return LINE;
    } else if(modeName == "WORD") {
        return WORD;
    } else if(modeName == "CHAR") {
        return CHAR;
    }
    
    return AUTO;
}

string OCR::findText(const Image& img) {
    Rectf roi(0, 0, img.getWidth(), img.getHeight());
    return findText(img, roi);
}

string OCR::findText( const Image& img, const Rectf& roi) {
    return findText(img.buf, img.getWidth(), img.getHeight(), img.bytesPerPixel, roi);
}

std::string OCR::findText( const unsigned char * data, int w, int h, int bpp, const Rectf & r )
{
    tess.Clear();
    return tess.TesseractRect(
              data,
              bpp,
              w * bpp,
              r.l, r.t,
              r.getWidth(), r.getHeight()
              );
}

float OCR::getConfidenceFor( const Image & img, const std::string & str)
{
    tess.Clear();
    tess.SetVariable("save_blob_choices", "T");
    tess.SetVariable("tessedit_char_whitelist", str.c_str());
    tess.SetImage(
                  img.buf,
                  img.getWidth(),
                  img.getHeight(),
                  img.bytesPerPixel,
                  img.getWidth() * img.bytesPerPixel
                  );
    
    Rectf r = Rectf(0,0,img.getWidth(),img.getHeight());
    tess.SetRectangle(r.l, r.t, r.getWidth(), r.getHeight());
    tess.Recognize(NULL);
    
    vectorf res;
    
    tesseract::ResultIterator* it = tess.GetIterator();
    
    if(!it)
        return 0.0f;
    
    char* uval = it->GetUTF8Text(tesseract::RIL_SYMBOL);
    return it->Confidence(tesseract::RIL_SYMBOL);//tesser)
    
    if(!uval)
        return 0.0f;
    
    cout<<uval<<"("<<it->Confidence(tesseract::RIL_SYMBOL)<<"){";
    
    tesseract::ChoiceIterator ci(*it);
    do {
        const char* val = ci.GetUTF8Text();
        float conf = ci.Confidence();
        res.push_back(conf);
        cout<<" "<<(val == NULL ? "#" : val)<<" "<<conf;
        printf("\n");
    } while (ci.Next());
    cout<<"}";
    
    return 0.0;//res;
}


int OCR::getMeanConfidence()
{
    return tess.MeanTextConf();
}
/*
string ofxTesseract::findText(ofImage& img, ofRectangle& roi) {
    ofPixels& pixels = img.getPixelsRef();
    int bytesPerPixel = pixels.getBytesPerPixel();
    
    tess.SetImage(
                  pixels.getPixels(),
                  img.getWidth(),
                  img.getHeight(),
                  bytesPerPixel,
                  pixels.getWidth() * bytesPerPixel
                  );
    
    tess.SetRectangle(
                      roi.x, roi.y,
                      roi.width, roi.height
                      );
    tess.Recognize()
    return tess.GetHOCRText(0);
}
*/
/*
 mTess.Recognize(0);
 tesseract::ResultIterator* ri = mTess.GetIterator();
 if(ri != 0)
 {
 do
 {
 const char* word = ri->GetUTF8Text(tesseract::RIL_WORD);
 if(word != 0 )
 {
 float conf = ri->Confidence(tesseract::RIL_WORD);
 printf("  word:%s, confidence: %f", word, conf );
 }
 delete[] word;
 } while((ri->Next(tesseract::RIL_WORD)));
 
 delete ri;
 }
 */


/*
 api.SetVariable("save_blob_choices","T");
 >   api.SetPageSegMode(tesseract::PSM_AUTO);
 >   api.SetImage(pixs);
 >   rc=api.Init(argv[0],lang);
 >   api.Recognize(NULL);
 >   tesseract::ResultIterator* it = api.GetIterator();
 >
 >   if(it!=0)
 >   {
 >   do
 >   {
 >   const char* symbol = it->GetUTF8Text(tesseract::RIL_SYMBOL);
 >   if(symbol!=0)
 >   {
 >
 >   float confi=it->Confidence(tesseract::RIL_SYMBOL);
 >   const tesseract::ResultIterator itr=*it;
 >    }
 >   delete[] symbol;
 >   } while((it->Next(tesseract::RIL_SYMBOL)));
 >   }
 
*/