//
//  genetic.cpp
//  iggi_ai_gen
//
//  Created by Daniel Berio on 10/03/15.
//  Copyright (c) 2015 colormotor. All rights reserved.
//

#include "genetic.h"
#include "Plot.h"

Genotype::Genotype( Env * env )
:
env(env)
{
    randomize();
}

Vec2 Genotype::mutatePos( const Vec2 & p, float amt )
{
    Vec2 res = p;
    Vec2 o;
    
    o.unitRand();
    o *= env->rect.getDiagonalLength() * frand(0.0,1);
    
    o.x = frand(env->rect.l, env->rect.r) - p.x;
    o.y = frand(env->rect.t, env->rect.b) - p.y;
    res =  p + o*env->mutateAmt*amt;
    res.x = clamp(res.x, env->rect.l, env->rect.r);
    res.y = clamp(res.y, env->rect.t, env->rect.b);
    
    float divx = env->rect.getWidth()/env->subdivisionX;
    float divy = env->rect.getHeight()/env->subdivisionY;
    
    //res.x = discretize(res.x, divx);
    //res.y = discretize(res.y, divy);
    
//    res.x = discretize(frand(env->rect.l, env->rect.r),divx);
 //   res.y = discretize(frand(env->rect.t, env->rect.b),divy);
    
    return res;
    // TODO try offsets.
}

void Genotype::mutate( float amt )
{
    if( frand() <= env->mutateProb )
        for( int i = 0; i < genes.rows(); i++ )
        {
            vector2f & row = genes[i];
            for( int i = 0; i < row.size(); i++ )
                row[i] = mutatePos(row[i], amt );
        }
}

Genotype Genotype::crossoverSingle( const Genotype & mate ) const
{
    // reproduce by swapping a random stroke
    Genotype child = *this;
    
    int isrc = random(0, mate.genes.rows());
    int idst = random(0, child.genes.rows());
    child.genes[idst] = mate.genes[isrc];
    return child;
}

Genotype Genotype::crossoverMulti( const Genotype & mate ) const
{
    // reproduce by swapping a random stroke
    Genotype child = *this;
    
    int n = std::min(child.genes.rows(), mate.genes.rows());
    for( int i = 0; i < n; i++ )
    {
        if(percent(50)) // could use fitness based probability here.
            child.genes[i] = mate.genes[i];
    }

    return child;
}

vector2f Genotype::randomRow( )
{
    int n = random(env->minStrokePts, env->maxStrokePts);
    vector2f res(n);
    for( int i = 0; i < n; i++ )
    {
        res[i].x = frand(env->rect.l, env->rect.r);
        res[i].y = frand(env->rect.t, env->rect.b);
    }
    return res;
}

void Genotype::randomize()
{
    genes.clear();
    int n = random(env->minStrokes, env->maxStrokes);
    for( int i = 0; i < n; i++ )
        genes.addRow( randomRow() );
}

///////////////////////////////////////////////////
///////////////////////////////////////////////////

Glyph::Glyph( Env * env )
:
env(env),
gen(env)
{
    img.create(env->rect.getWidth(), env->rect.getHeight(), Image::GRAYSCALE);
    update();
}

Glyph::~Glyph()
{
}
    
float Glyph::fittness()
{
    update();
    
    float conf = env->ocr.getConfidenceFor(img,env->characters);

    conf /= 100.0;
    // use a power function because Tesseract is quite kind with confidence
    conf = pow(conf, env->confPower);
    
    // store confidence
    confidence = conf;
    
    // Add a cost for glyphs that are too small
    Rectf ir = env->rect;
    Rectf sr = strokes.boundingBox();
    
    // reference diagonal length
    float dr = (ir.getDiagonalLength())*env->minAreaScale;
    // diag length of glyph
    float dg = sr.getDiagonalLength();
    
    float sizecost = 1.0 / fabs(dr-dg);
    
    return conf * sizecost;
//    return conf * sizecost; //pow(ia/sa, 3.0); //*(sa/ia);
    
}

void Glyph::update()
{
    strokes.clear();
    for( int i = 0; i < gen.genes.rows(); i++ )
    {
        strokes.addContour(Contour(gen.genes[i]));
    }
    
    // scale it
    /*
    Rectf r = env->rect;
    r.scaleAroundCenter(0.6,0.6);
    Rectf box = strokes.boundingBox();
    M33 fit = getFittingMatrix(box, r, true);
    strokes.transform(fit);
    */
    
    // fill image
    img.clear();
    img.lineWidth(env->lineWidth);
    img.color(Color::white());
    for( int i = 0; i < strokes.size(); i++ )
        img.drawContour(strokes[i].points);
    img.invert();
    img.blur(env->blurAmt);
}

void Glyph::draw()
{
    gfx::color(1.0f);
    img.draw(0,0,512,512);
    
    gfx::color(1,0,0);
    for( int i = 0; i < strokes.size(); i++ )
        strokes[i].draw();
    
}
///////////////////////////////////////////////////
///////////////////////////////////////////////////


Population::Population( Env * env )
:
env(env),
bestGen(env),
bestFitness(-1.0)
{
    for( int i = 0; i < numElements; i++ )
        elements.push_back(Glyph(env));
}

Population::~Population()
{
    
}

void Population::fitness()
{
    F.assign(elements.size(), 0.0f);
    for( int i = 0; i < elements.size(); i++ )
        F[i] = elements[i].fittness();
}

template <class T>
T map( const T &v, const T & srcLow, const T & srcHi, const T & dstLow, const T & dstHi  )
{
    return dstLow + ((v - srcLow) / (srcHi - srcLow))*(dstHi - dstLow);
}
/*
void Population::selection()
{
    float maxFitness = max(F);
    float meanFitness = mean(F);
    
    fmaxHistory.push_back(maxFitness);
    fmeanHistory.push_back(meanFitness);
    if(fmaxHistory.size() > 100)
    {
        fmaxHistory.erase(fmaxHistory.begin());
        fmeanHistory.erase(fmeanHistory.begin());
    }
    
    matingPool.clear();
    
    for( int i = 0; i < F.size(); i++ )
    {
        float f = map( F[i], 0.0f , maxFitness, 0.0f, 1.0f );
        printf("%g\n",f);
        int n = (int) (f * 100);
        if( n < 1 )
        {
            n = 1;
            
        }
        
        for ( int j = 0; j < n; j++ )
        {
            matingPool.push_back(i);
        }

        printf ("%d in pool\n",matingPool.size());
    }
}
*/

void Population::selection()
{
    float maxFitness = max(F);
    float meanFitness = mean(F);
    
    fmaxHistory.push_back(maxFitness);
    fmeanHistory.push_back(meanFitness);
    if(fmaxHistory.size() > 100)
    {
        fmaxHistory.erase(fmaxHistory.begin());
        fmeanHistory.erase(fmeanHistory.begin());
    }
    
    matingPool.clear();
    
    for( int i = 0; i < F.size(); i++ )
    {
        float e = F[i] / meanFitness;
        int n = (int)floorf(e);
        float p = e-n;
        if(frand() <= p)
            n++;
        
        for ( int j = 0; j < n; j++ )
        {
            matingPool.push_back(i);
        }
        
        
    }
    
    printf ("%d in pool\n",matingPool.size());
    
    if(!matingPool.size())
    {
        assert(0);
    }
}


void Population::reproduction()
{
    float fmean = fmeanHistory.back();
    
    // Keep the best specimen around (Not)
    int besti = maxIndex(F);
    if(F[besti] > bestFitness)
    {
        bestFitness = F[besti];
        bestGen = elements[besti].gen;
        //env->choosen.push_back(elements[besti].strokes);
    }
    else
    {
        bestFitness *= 0.9999;
    }
    //elements[0].gen = bestGen;
    
    for (int i = 0; i < elements.size(); i++)
    {
        {
            int m = random( 0, matingPool.size() );
            int d = random( 0, matingPool.size() );
            
            // Pick two parents
            const Glyph &mom = elements[matingPool[m]];
            const Glyph &dad = elements[matingPool[d]];
            
            Genotype child = (percent(50))? mom.gen.crossoverMulti( dad.gen ) : dad.gen.crossoverMulti( mom.gen );
            //if( F[i] < frand() < 1.0-(F[matingPool[m]]+F[matingPool[d]])/2 )
            child.mutate(1.0 - (mom.confidence+dad.confidence)/2);// elements[i].confidence);
            
            elements[i].gen = child;
        }
        //population[i] = new Rocket(location, child,population.length);
    }
    
    generations++;
}


void Population::draw()
{
    /*
    for( int i = 0; i < elements.size(); i++ )
        elements[i].draw();*/
    if(!F.size())
        return;
    
    int i = maxIndex(F);
    elements[i].draw();
    
    if( elements[i].confidence > env->choiceThresh )
    {
        bool same = (env->choosen.size() && env->choosen.back() == elements[i].strokes );
        if(!same)
            env->choosen.push_back(elements[i].strokes);
    }
    
    gfx::color(1.0);
    drawText(20,600,"generations: %d",generations);
    gfx::color(1.0,0,0);
    if(fmaxHistory.size())
    {
        gfx::color(Color::red());
        drawText(20,630,"Max Fittness: %g",fmaxHistory.back());
        gfx::color(Color::turquoise());
        drawText(20,650,"Mean Fittness: %g",fmeanHistory.back());

        Rectf r(20,700,3*fmaxHistory.size(),150);
        gfx::color(0.5);
        plotData( fmaxHistory, r, Color::red(), 0.0, env->plotScale );
        plotData( fmeanHistory, r, Color::turquoise(), 0.0, env->plotScale );
    }
}

void Population::reset()
{
    fmaxHistory.clear();
    fmeanHistory.clear();
    bestFitness = -1;
    
    for( int i = 0; i < elements.size(); i++ )
        elements[i].gen.randomize();
}

///////////////////////////////////////////////////
///////////////////////////////////////////////////
Env::Env()
:
population(this),
params("Genetic Environment"),
rect(0,0,64,64)
{
    strcpy(characters,"R");
    params.addCString("Characters",characters);
    params.addInt("Min Strokes", &minStrokes);
    params.addInt("Max Strokes", &maxStrokes);
    params.addSeparator();
    params.addInt("Min Stroke Pts", &minStrokePts);
    params.addInt("Max Stroke Pts", &maxStrokePts);
    params.addSeparator();
    params.addFloat("Mutate Prob",&mutateProb,0.0,1.0);
    params.addFloat("Mutate Amt",&mutateAmt,0.0,1.0);
    params.addSeparator();
    params.addFloat("Line Width",&lineWidth,1.0,30.0);
    params.addFloat("Conf Power",&confPower,1.0,20.0);
    params.addFloat("Min Area Scale",&minAreaScale,0.01,2.0);
    params.addFloat("Blur Amt",&blurAmt,0,3);
    params.addInt("Subd X",&subdivisionX);
    params.addInt("Subd Y",&subdivisionY);
    params.addFloat("Choice Thresh",&choiceThresh,0.1,1.0);
    params.addEvent("Reset",reset_);
    params.addFloat("Plot Scale",&plotScale,0.01,1.0);
}
    
bool Env::init()
{
    ocr.setup("eng.traineddata");
    ocr.setWhitelist("FAR"); // not necessary
    ocr.setMode(OCR::CHAR);
    
    for( int i = 0; i < population.elements.size(); i++ )
        population.elements[i].gen.randomize();
    return true;
}

void Env::update()
{
    if(reset_.isTriggered())
        reset();
    
    population.fitness();
    population.selection();
    population.reproduction();
}

void Env::drawChoices( float x, float y, float canvasw  )
{
    float w = rect.getWidth();
    
    int n = (canvasw/w)-1;
    for( int i = 0; i < choosen.size(); i++ )
    {
        int ix = i%n;
        int iy = i/n;
        
        gfx::pushMatrix();
        gfx::translate(x+ix*w, y+iy*w);
//        gfx::scale(s);
        choosen[i].draw();
        gfx::popMatrix();
    }
}

void Env::drawChoicesToEps( const std::string & path, float x, float y, float canvasw  )
{
    EpsFile eps;
    eps.open(path);
    eps.header(0,0,2000,2000);
    
    float w = rect.getWidth();
    
    int n = (canvasw/w)-1;
    for( int i = 0; i < choosen.size(); i++ )
    {
        int ix = i%n;
        int iy = i/n;
        
        M33 m;
        m.identity();
        m.translate(x+ix*w, y+iy*w);
        Shape2d s = shape2dFromShape(choosen[i]);
        s.transform(m);
        eps.strokeShape(s, Color::black());
    }
    
    eps.close();
}

void Env::reset()
{
    population.reset();
    choosen.clear();
}
