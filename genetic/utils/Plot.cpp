#include "Plot.h"
#include "Shape.h"

namespace cm
{
    static float yp( float y, float min_y, float max_y, const Rect & rect )
    {
        return (rect.b - ((y - min_y) / (max_y - min_y))*rect.getHeight());
    }
    
    #define YP(y) yp( y, min_y, max_y, rect )
    
    void plotData( const vectorf & Y, const Rectf & rect, const Color & clr, float min_y, float max_y)
    {
        if( min_y == 0.0 && max_y == 0.0)
        {
            min_y = min(Y);
            max_y = max(Y);
        }
        

        
        gfx::color(clr);
        gfx::beginVertices(gfx::LINESTRIP);
        vectorf XP = linspace(rect.l, rect.r, Y.size());
        for( int i = 0; i < XP.size(); i++ )
            gfx::vertex(XP[i],YP(Y[i]));
        gfx::endVertices();
    }
    
    void plotDataFilled( const vectorf & Y, const Rectf & rect, const Color & clr, float min_y, float max_y)
    {
        if( min_y == 0.0 && max_y == 0.0)
        {
            min_y = min(Y);
            max_y = max(Y);
        }
        
        Contour p;
        vectorf XP = linspace(rect.l, rect.r, Y.size());
        for( int i = 0; i < XP.size(); i++ )
            p.addPoint(XP[i],YP(Y[i]));
        p.addPoint(p.last().x,p[0].y); // this is to make it even...
        p.close();
        
        gfx::color(clr.withAlpha(0.5));
        p.fill();
        gfx::color(clr);
        p.draw();

    }
    
    void plotFeatures( const std::vector<int> &I, const vectorf & Y, const Rectf & rect, const Color & clr )
    {
        
        for( int i = 0; i < I.size(); i++ )
        {
            gfx::color(clr);
            int f = I[i];
            float t = (float)f / Y.size();
            float x = rect.l + rect.getWidth()*t;
            fillCircle(Vec2(x,rect.b), 4);
            gfx::color(clr.withAlpha(0.5));
            gfx::drawLine(Vec2(x,rect.b), Vec2(x,rect.t));
        }
    }
    
    void plotFeatures( const std::vector<pairi> & F, const vectorf & X, const Rectf & rect, const Color & clr )
    {
         gfx::color(clr);
        
        for( int i = 0; i < F.size(); i++ )
        {
            const pairi & f = F[i];
            int xa = f.first;
            int xb = f.second;
            float ta = (float)xa / X.size();
            float tb = (float)xb / X.size();
            Vec2 a = Vec2(rect.l + rect.getWidth()*ta, rect.b);
            Vec2 b = Vec2(rect.l + rect.getWidth()*tb, rect.b);
            gfx::drawArrow(a,b);
            gfx::drawArrow(b,a);
        }
    }
    
    void plotBarsAtFeatures( const Rectf & rect, const vectorf & Y, const vectori & I, int n, const Color & clr, float min_y, float max_y )
    {
        gfx::color(clr);
        gfx::lineWidth(3.0);
        for( int i = 0; i < I.size(); i++ )
        {
            float v = Y[i];
            
            float tx = (float)I[i] / n;
            float x = rect.l + tx*rect.getWidth();
            float y = YP(v);
            
            gfx::drawLine(Vec2(x,rect.b),Vec2(x,y));
            
        }
        gfx::lineWidth(1.0);
    }
    
    void drawZero( const Rectf & rect, float min_y, float max_y )
    {
        float y = YP(0.0);
        gfx::drawLine(Vec2(rect.l, y), Vec2(rect.r, y));
    }

    void drawZero( const Rectf & rect, const vectorf & Y )
    {
        float min_y = min(Y);
        float max_y = max(Y);
        float y = YP(0.0);
        gfx::drawLine(Vec2(rect.l, y), Vec2(rect.r, y));
    }
    
    void drawYLabels( const Rectf & r, float min_y, float max_y, float offset )
    {
        drawText(r.l - offset, r.b, "%g", min_y );
        drawText(r.l - offset, r.t, "%g", max_y );
    }
    
    void drawYLabels( const vectorf & Y, const Rectf & r, float offset )
    {
        float min_y = min(Y);
        float max_y = max(Y);
        drawText(r.l - offset, r.b, "%g", min_y );
        drawText(r.l - offset, r.t, "%g", max_y );
    }
    
    void drawXLabels( const Rectf & r, float min_x, float max_x )
    {
        drawText( r.l, r.b, "%g", min_x );
        drawText( r.r, r.b, "%g", max_x );
    }
    
}