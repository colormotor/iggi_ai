#pragma once
#include "colormotor.h"
#include "numeric.h"

namespace cm
{
    void plotData( const vectorf & Y, const Rectf & rect, const Color & clr = Color::black(), float min_y = 0.0, float max_y = 0.0);
    void plotDataFilled( const vectorf & Y, const Rectf & rect, const Color & clr = Color::black(), float min_y = 0.0, float max_y = 0.0);
    void plotFeatures( const std::vector<int> &I, const vectorf & Y, const Rectf & rect, const Color & clr = Color::black() );
    void plotFeatures( const std::vector<pairi> & F, const vectorf & Y, const Rectf & rect, const Color & clr = Color::red() );
    void drawZero( const Rectf & rect, float min_y, float max_y );
    void drawZero( const Rectf & rect, const vectorf & Y );
    void drawYLabels( const Rectf & r, float min_y, float max_y, float offset=20 );
    void drawYLabels( const vectorf & Y, const Rectf & r, float offset=20 );
    void drawXLabels( const Rectf & r, float min_x, float max_x );
    void plotBarsAtFeatures( const Rectf & r, const vectorf & Y, const vectori & I, int n, const Color & clr, float min_y, float max_y );
    
}