#include "colormotor.h"
//#include "ZoomyApp.h"
#include "BetaLognormal.h"
#include "morpho.h"
#include "Shape.h"
#include "Plot.h"
#include "ImGuiApp.h"
#include "BetaLognormalFitter.h"
#include "utils.h"
#include "genetic.h"
#include "ocr.h"

using namespace cm;

float dummy = 0.0;


bool bCompute = true;
bool bTest = false;
Trigger<bool> reloadImg_;
Trigger<bool> saveShapes_;
Image img;

Env env;

OCR ocr;

CriticalSection cs;

Trigger<bool> saveEps_;

class App : public ImGuiApp
{
	public:
		App()
		{
            params.addEvent("Save Eps..",saveEps_);
            params.addBool("Compute",&bCompute);
            params.addBool("Test",&bTest);
            
            params.addChild(&env.params);
            params.addEvent("Reload Img",reloadImg_);
            params.addEvent("Save Shapes...",saveShapes_);
            
            addParams(params,"Params");
            
            paramsToSave.addParams(params);
		}
    
    

		~App() 
		{

		}
    
    
    
        void customGui()
        {
        }
    
    
        bool init()
		{
            setManualGLRelease(true);
            img.load("test.png", Image::RGB);
            env.init();
            
            
            ocr.setup("eng.traineddata");
//            ocr.setWhitelist("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
///            tess.setAccuracy(ofxTesseract::ACCURATE);
            ocr.setMode(OCR::CHAR);
            
            std::vector<Shape2d> ss = shapes2dFromXml("glyphs.xml");
            printf("%d shapes\n",ss.size());
            return true;
        }

		void draw()
		{
            cs.lock();
            
//            randomseed(seed);
            gfx::clear(0,0,0,0);
            gfx::setOrtho(viewport.width,viewport.height);
            gfx::color(1.0);
            
            if( bTest )
            {
                img.draw(0,0,512,512);
                float conf = ocr.getConfidenceFor(img,env.characters);
                float fittness = pow( conf/100, env.confPower );
                drawText(20,600,"Conf: %g fittness: %g",conf,fittness);
                return;
            }
            
            if(bCompute)
            {
                env.update();
            }
            
            env.population.draw();
            
            gfx::color(Color::magenta());
            float x = 20;
            
            bool save = false;
            if(saveEps_.isTriggered())
            {
                std::string path;
                if(saveFileDialog(path,"eps"))
                {
                    env.drawChoicesToEps(path,20,20,viewport.width);
                }
            }
            
            env.drawChoices(x,100,viewport.width);
            
             /*
            std::string str = "Cazzo";//ocr.findText(img);
            printf("found: %s\n",str.c_str());
            env.population.draw();
            float conf = ocr.getConfidenceFor(img,"A");
            drawText(20,400,"Found: %s Conf %g",str.c_str(),conf);//ocr.getMeanConfidence());
             */
            
            cs.unlock();
		}
    


		void update()
		{
            if(reloadImg_.isTriggered())
            {
                img.load("test.png", Image::GRAYSCALE);
            }
            
            if(saveShapes_.isTriggered())
            {
                std::string path = "glyphs.xml";
                cs.lock();
                // This stopped working..... damn
                //if(saveFileDialog(path, "xml"))
                saveXmlFile( path, shapesToXml(env.choosen) );
                cs.unlock();
            }
		}

		void exit()
		{
		}
    
        void    mouseMoved( int x, int y )
        {
            return;
        }
    
		void	mouseDragged(int x, int y, int btn)
        {
            return;
        }
    
        void	mousePressed( int x, int y, int btn )
        {
            return;
        }
        
        void	mouseReleased( int x, int y, int btn )
        {
            return;
        }
    
        void    doubleClick( int x, int y )
        {
            return;
        }
        void    ascii(int k)
        {
        }

		
	protected:	
};





int main(int argc, char **argv)
{
    App app;
    runGlfwApp( &app, 1400,968, false, "capture" );
    return 0;
}
