#pragma once
#include "colormotor.h"
#include "tesseract/baseapi.h"
#include "numeric.h"


// Based on ofxTesseract
class OCR
{
public:
    OCR();
    
    enum Mode {
        AUTO = tesseract::PSM_AUTO,
        COLUMN = tesseract::PSM_SINGLE_COLUMN,
        BLOCK = tesseract::PSM_SINGLE_BLOCK,
        LINE = tesseract::PSM_SINGLE_LINE,
        WORD = tesseract::PSM_SINGLE_WORD,
        CHAR = tesseract::PSM_SINGLE_CHAR
    };
    
    int getMeanConfidence();
    
    // dataPath is where to find a folder with tessdata inside
    // default arguments assume bin/data/tessdata/eng.traineddata
    void setup( std::string dataPath = "", bool absolute = false, std::string language = "eng");
    
    // you must call these after setup, contrary to the tesseract-ocr docs
    // whitelist means only those characters are used
    void setWhitelist( std::string whitelistCharacters);
    
    // blacklist means none of those characters are used
    void setBlacklist( std::string blacklistCharacters);
    
    // provides a hint for how to segment things
    // by default this is ofxTesseract::AUTO
    void setMode(Mode mode);
    
    // transform a mode name into a constant, for loading from XML files
    static Mode getMode( std::string modeName);
    
    // finally, do OCR on an image, or on an image within an roi
    // make sure your text is at least 10 pixels tall
    // OF_IMAGE_COLOR and OF_IMAGE_GRAYSCALE are both fine
    std::string findText( const Image& img);
    std::string findText( const Image& img, const Rectf& roi);
    
    std::string findText( const unsigned char * data, int w, int h, int bpp, const Rectf & r );
    
    float getConfidenceFor( const Image & img, const std::string & str);
    //HOCR goodness
    //std::string findTextHocr(Image& img);
    //std::string findTextHocr(Image& img, Rectf& roi);
    
protected:
    tesseract::TessBaseAPI tess;
};

